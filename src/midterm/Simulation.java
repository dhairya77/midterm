/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package midterm;

import java.util.List;

/**
 *
 * @author DHAIRYA Kachhia
 */
public class Simulation {
    private String name;
    private String quantitiy;
    private String id;
    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Simulation(List<Item> items) {
        this.items = items;
    }

    public Simulation(String name, String quantitiy, String id) {
        this.name = name;
        this.quantitiy = quantitiy;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantitiy() {
        return quantitiy;
    }

    public void setQuantitiy(String quantitiy) {
        this.quantitiy = quantitiy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void printItem()
    {
        System.out.println();
    }
    
    
}
